/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('am-wb-seen-calendar')
/**
 * @ngdoc Controllers
 * @name AmWbSeenCalendarCtrl
 * @description A calendar controller
 * 
 * Get events from server
 * 
 */
.controller('AmWbSeenCalendarCtrl', function($scope, $filter) {

	$scope.dayFormat = 'd';

	// To select a single date, make sure the ngModel is not an array.
	$scope.selectedDate = null;

	// If you want multi-date select, initialize it as an array.
	$scope.selectedDate = [];

	$scope.firstDayOfWeek = 0; // First day of the week, 0 for Sunday, 1 for Monday, etc.
	$scope.setDirection = function(direction) {
		$scope.direction = direction;
		$scope.dayFormat = direction === 'vertical' ? 'EEEE, MMMM d' : 'd';
	};

	$scope.dayClick = function(date) {
		$scope.msg = 'You clicked ' + $filter('date')(date, 'MMM d, y h:mm:ss a Z');
	};

	$scope.prevMonth = function(data) {
		$scope.msg = 'You clicked (prev) month ' + data.month + ', ' + data.year;
	};

	$scope.nextMonth = function(data) {
		$scope.msg = 'You clicked (next) month ' + data.month + ', ' + data.year;
	};

	$scope.tooltips = true;
	$scope.setDayContent = function(/*date*/) {

		// You would inject any HTML you wanted for
		// that particular date here.
		return '<p></p>';

		// You could also use an $http function directly.
//		return $http.get('/some/external/api');

		// You could also use a promise.
//		var deferred = $q.defer();
//		$timeout(function() {
//		deferred.resolve('<p></p>');
//		}, 1000);
//		return deferred.promise;

	};

});
