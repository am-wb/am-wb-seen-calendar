# Angular material calender for WB

[![pipeline status](https://gitlab.com/am-wb/am-wb-seen-calendar/badges/master/pipeline.svg)](https://gitlab.com/am-wb/am-wb-seen-calendar/commits/master)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/f376c4b7bc264ad8b014934a53719cba)](https://www.codacy.com/app/am-wb/am-wb-seen-calendar?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=am-wb/am-wb-seen-calendar&amp;utm_campaign=Badge_Grade)



See more details in [technical document](https://am-wb.gitlab.io/am-wb-calendar/doc/index.html).