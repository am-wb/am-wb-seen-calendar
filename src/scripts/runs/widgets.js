/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('am-wb-seen-calendar')
/**
 * @ngdoc Runs
 * @name Widgets
 * @description Defines and registers some widgets. Following widgets are registered by this module:
 * 
 * - SeenCalendar
 * - SeenEventList
 * 
 */
.run(function($widget) {
	$widget.newWidget({
		// info
		type : 'SeenCalendar',
		title : 'Seen Calendar',
		description : 'Calendar',
		icon : 'calendar',
		// run
		controller : 'AmWbSeenCalendarCtrl', 
		controllerAs: 'ctrl',
		templateUrl : 'views/am-wb-seen/calendar.html',
		setting : [ ],
		data : {
			type : 'SeenCalendar',
			title : 'Seen calendar',
			description: 'System calendar'
		},
		// help
		link : 'https://gitlab.com/weburger/am-wb-seen-calendar'
	});
	$widget.newWidget({
		// info
		type : 'SeenEventList',
		title : 'Event list',
		description : 'List of events',
		icon : 'event',
		// run
		controller : 'AmWbSeenEventsCtrl',
		controllerAs: 'ctrl',
		templateUrl : 'views/am-wb-seen/events.html',
		setting : [ ],
		data : {
			type : 'SeenEventList',
			title : 'events',
			description: 'event list'
		},
		// help
		link : 'https://gitlab.com/weburger/am-wb-seen-calendar'
	});
});
