/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('am-wb-seen-calendar',[ 
    'am-wb-seen-core',
    'materialCalendar'
]);

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict'; 

//Test controller
angular.module('am-wb-seen-calendar')
//تنظیم ایکون
.config(function(wbIconServiceProvider) {
	wbIconServiceProvider//
	.addShape('calendar', '<path d="M20 3h-1V1h-2v2H7V1H5v2H4c-1.1 0-2 .9-2 2v16c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm0 18H4V8h16v13z"/>');//
});
/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('am-wb-seen-calendar')
/**
 * @ngdoc Controllers
 * @name AmWbSeenCalendarCtrl
 * @description A calendar controller
 * 
 * Get events from server
 * 
 */
.controller('AmWbSeenCalendarCtrl', function($scope, $filter) {

	$scope.dayFormat = 'd';

	// To select a single date, make sure the ngModel is not an array.
	$scope.selectedDate = null;

	// If you want multi-date select, initialize it as an array.
	$scope.selectedDate = [];

	$scope.firstDayOfWeek = 0; // First day of the week, 0 for Sunday, 1 for Monday, etc.
	$scope.setDirection = function(direction) {
		$scope.direction = direction;
		$scope.dayFormat = direction === 'vertical' ? 'EEEE, MMMM d' : 'd';
	};

	$scope.dayClick = function(date) {
		$scope.msg = 'You clicked ' + $filter('date')(date, 'MMM d, y h:mm:ss a Z');
	};

	$scope.prevMonth = function(data) {
		$scope.msg = 'You clicked (prev) month ' + data.month + ', ' + data.year;
	};

	$scope.nextMonth = function(data) {
		$scope.msg = 'You clicked (next) month ' + data.month + ', ' + data.year;
	};

	$scope.tooltips = true;
	$scope.setDayContent = function(/*date*/) {

		// You would inject any HTML you wanted for
		// that particular date here.
		return '<p></p>';

		// You could also use an $http function directly.
//		return $http.get('/some/external/api');

		// You could also use a promise.
//		var deferred = $q.defer();
//		$timeout(function() {
//		deferred.resolve('<p></p>');
//		}, 1000);
//		return deferred.promise;

	};

});

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('am-wb-seen-calendar')
/**
 * @ngdoc Controllers
 * @name AmWbSeenEventsCtrl
 * @description The controller fo events
 * 
 */
.controller('AmWbSeenEventsCtrl', function($scope, $calendar) {
    $calendar.events()//
    .then(function(events){
	$scope.events = events;
    });
});

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('am-wb-seen-calendar')
/**
 * @ngdoc Runs
 * @name Widgets
 * @description Defines and registers some widgets. Following widgets are registered by this module:
 * 
 * - SeenCalendar
 * - SeenEventList
 * 
 */
.run(function($widget) {
	$widget.newWidget({
		// info
		type : 'SeenCalendar',
		title : 'Seen Calendar',
		description : 'Calendar',
		icon : 'calendar',
		// run
		controller : 'AmWbSeenCalendarCtrl', 
		controllerAs: 'ctrl',
		templateUrl : 'views/am-wb-seen/calendar.html',
		setting : [ ],
		data : {
			type : 'SeenCalendar',
			title : 'Seen calendar',
			description: 'System calendar'
		},
		// help
		link : 'https://gitlab.com/weburger/am-wb-seen-calendar'
	});
	$widget.newWidget({
		// info
		type : 'SeenEventList',
		title : 'Event list',
		description : 'List of events',
		icon : 'event',
		// run
		controller : 'AmWbSeenEventsCtrl',
		controllerAs: 'ctrl',
		templateUrl : 'views/am-wb-seen/events.html',
		setting : [ ],
		data : {
			type : 'SeenEventList',
			title : 'events',
			description: 'event list'
		},
		// help
		link : 'https://gitlab.com/weburger/am-wb-seen-calendar'
	});
});

angular.module('am-wb-seen-calendar').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/am-wb-seen/calendar.html',
    "<calendar-md flex layout layout-fill calendar-direction=direction on-prev-month=prevMonth on-next-month=nextMonth on-day-click=dayClick title-format=\"'MMMM y'\" ng-model=selectedDate week-starts-on=firstDayOfWeek data-start-month=8 data-start-year=2014 tooltips=tooltips day-format=dayFormat day-label-format=\"'EEE'\" day-label-tooltip-format=\"'EEEE'\" day-tooltip-format=\"'fullDate'\" day-content=setDayContent disable-future-selection=false> </calendar-md>"
  );


  $templateCache.put('views/am-wb-seen/events.html',
    " <md-list flex> <md-list-item class=md-3-line ng-repeat=\"event in events.items\"> <md-icon class=md-avatar>archive</md-icon> <div class=md-list-item-text layout=column> <h3>{{event.title}}</h3> <h4>{{event.from|pdate:'jYYYY/jMM/jDD hh:mm'}}-{{event.to|pdate:'jYYYY/jMM/jDD hh:mm'}}</h4> <p>{{event.description}}</p> </div> </md-list-item> </md-list>"
  );

}]);
